using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ConfigRegistrationBase : MonoBehaviour
{
    public abstract void RegisterInScope(Scope scope); 
}
