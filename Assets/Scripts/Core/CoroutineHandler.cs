using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoroutineHandler : MonoBehaviour, ICoroutineHandler
{
    private List<(IEnumerator routine, Action callback)> requestQueue = new List<(IEnumerator, Action)>();

    public void RunCoroutine(IEnumerator routine, Action callback) {
        requestQueue.Add((routine, callback));
    }

     

    private void Update() {
        var toRun = requestQueue.ToArray();
        requestQueue.Clear();
        toRun.ForEach(x => {
            StartCoroutine(RoutineWithCallback(x.routine, x.callback));
        });
    }

    private IEnumerator RoutineWithCallback(IEnumerator routine, Action callback) {
        yield return routine;
        callback?.Invoke();
    }
}

public interface ICoroutineHandler
{
    void RunCoroutine(IEnumerator routine, Action callback);
}
