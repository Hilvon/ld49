using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate IObservable<bool> IsLevelPassed(int id);

public delegate void MarkLevelPassed(int id);

public delegate void StartLevelAfter(int id);

public delegate void ResetProgress();

public class CoreState
{
    public IObservable<int> OnLevelAdded => _levelAdded;

    private Subject<int> _levelAdded;

    List<int> PassedLevels = new List<int>();

    public CoreState() {
        PassedLevels = (List<int>)JsonUtility.FromJson(PlayerPrefs.GetString("MissionProgress"), typeof(List<int>));
        if (PassedLevels == null) 
            PassedLevels = new List<int>();

        _levelAdded = new Subject<int>();
    }

    public bool IsLevelPassed(int id) {
        return PassedLevels.Contains(id);
    }

    public void MarkLevelPassed(int id) {
        if (!IsLevelPassed(id)) {
            PassedLevels.Add(id);
            PlayerPrefs.SetString("MissionProgress", JsonUtility.ToJson(PassedLevels));
            PlayerPrefs.Save();
            _levelAdded.OnNext(id);
        }
    }

    public void Reset() {
        PassedLevels.Clear();
        PlayerPrefs.SetString("MissionProgress", JsonUtility.ToJson(PassedLevels));
        PlayerPrefs.Save();
    }
}
