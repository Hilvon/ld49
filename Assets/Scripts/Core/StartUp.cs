using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class StartUp : MonoBehaviour
{
    [SerializeField] private RectTransform UIRoot;
    [SerializeField] private ConfigRegistrationBase[] extras;
    // Start is called before the first frame update
    void Start()
    {
        var subviewBinder = new SubviewFactory();
        //subviewBinder.BindSubview(new LandingPageViewModel(), null, UIRoot, new DisposableCollection());*/
        var loadingScreen = new StartupUIViewModel();
        var root = new GameRoot(loadingScreen);
        var diposables = new DisposableCollection();
        subviewBinder.BindSubview(root.contentViewModel, null, null, diposables);
        subviewBinder.BindSubview(root.uiViewModel, null, UIRoot, diposables);
        var CoreScope = new Scope();
        extras.ForEach(x => x.RegisterInScope(CoreScope));
        CoreScope.RegisterExternal<IGameRoot>(root);
        CoreRegistrator.RegisterCoreScope(CoreScope);
        CoreScope.Resolve<StartApplication>()
            .Invoke(loadingScreen);
    }
}

public delegate Task StartApplication(ILoadingProgressTracker progressTracker);

public interface IGameRoot
{
    public void SetGameContent(object viewModel);
    public IUIControl uiViewModel { get; }
}

public delegate void SetupUI(IUIControl uiRoot);

public delegate object InitialContent();

public class GameRoot : DisposableCollection, IGameRoot
{
    public IObservable<object> contentViewModel => _content;
    public IUIControl uiViewModel { get; }


    private Subject<object> _content;

    public GameRoot(object initialUIcontent) {
        _content = new Subject<object>().AddTo(this);
        uiViewModel = new UIRootViewModel(initialUIcontent);
    }    

    public void SetGameContent(object viewModel) {
        _content.OnNext(viewModel);
    }
}