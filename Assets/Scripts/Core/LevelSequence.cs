using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LevelSequence: ILevelProcessor, INextLevelLocator
{
    private Dictionary<int, LevelDataHolder> UnlockedLevelsDictionary = new Dictionary<int, LevelDataHolder>();
    private LevelDataHolder[] allLevels;
    private Dictionary<int, int> Ranks = new Dictionary<int, int>();
    public IEnumerable<LevelDataHolder> Levels => allLevels;
    public void SetLevelsCollection(IEnumerable<LevelDataHolder> levels) {
        allLevels = levels.ToArray();
        levels.ForEach(lvl => {
            if (lvl.requiredLevelId != -1 && !UnlockedLevelsDictionary.ContainsKey(lvl.requiredLevelId)) {
                UnlockedLevelsDictionary.Add(lvl.requiredLevelId, lvl);
            }
        });
        allLevels.Where(x => x.requiredLevelId == -1).ForEach(x => AssingRankToLevel(x.ID, 1));
    }

    public LevelDataHolder FindNextLevel(int ID) {
        if (UnlockedLevelsDictionary.TryGetValue(ID, out var level)) {
            return level;
        }
        return null;
    }

    public int GetLevelRank(LevelDataHolder level) {
        return Ranks.TryGetValue(level.ID, out var rank) ? rank : -1;
    }

    private void AssingRankToLevel(int id, int rank) {
        Ranks.Add(id, rank);
        allLevels.Where(x => x.requiredLevelId == id).ForEach(x => AssingRankToLevel(x.ID, rank + 1));
    }
}

public interface ILevelProcessor
{
    void SetLevelsCollection(IEnumerable<LevelDataHolder> levels);
}

public interface INextLevelLocator
{
    LevelDataHolder FindNextLevel(int ID);
}

public delegate IEnumerable<LevelDataHolder> GetAllLevels();

public delegate int GetLevelRank(LevelDataHolder level);