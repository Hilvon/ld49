using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public interface ILandingPageViewModel : IDisposable {
    IObservable<float> progressBar { get; }
    IObservable<string> progressStatus { get; }
}
public class LandingPageViewModel: DisposableCollection, ILandingPageViewModel
{
    public IObservable<float> progressBar { get; }
    public IObservable<string> progressStatus { get; }

    public LandingPageViewModel() {
        progressBar = new Subject<float>().AddTo(this);
        progressStatus = new Subject<string>().AddTo(this);
    }

    
}
