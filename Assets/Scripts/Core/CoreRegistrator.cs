using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public static class CoreRegistrator 
{
    public static void RegisterCoreScope(Scope coreScope) {

        var levelSequnce = new LevelSequence();
        coreScope.RegisterScoped<ILevelProcessor>(c => levelSequnce);
        coreScope.RegisterScoped<INextLevelLocator>(c => levelSequnce);
        coreScope.RegisterScoped<GetAllLevels>(c => () => levelSequnce.Levels);
        coreScope.RegisterScoped<GetLevelRank>(c => levelSequnce.GetLevelRank);
        var coreState = new CoreState();
        coreScope.RegisterScoped<MarkLevelPassed>(c => coreState.MarkLevelPassed);
        coreScope.RegisterScoped<IsLevelPassed>(c => 
            levelId => levelId ==-1 ? Observable.Return(true) : coreState.OnLevelAdded.Select(_=>coreState.IsLevelPassed(levelId))
        );
        coreScope.RegisterScoped<ResetProgress>(c => coreState.Reset);

        coreScope.RegisterScoped<StartApplication>(c =>
            async (progressTracker) => {
                progressTracker.SetProgressAmount(0);
                progressTracker.SetStatus("reading level data");
                var loadLevels = Resources.LoadAll<LevelDataHolder>("");
                c.Resolve<ILevelProcessor>().SetLevelsCollection(loadLevels);
                progressTracker.SetProgressAmount(1);
                progressTracker.SetStatus("openingMenu");

                await c.Resolve<StartMenu>().Invoke();
        });

        coreScope.RegisterScoped<RunGameplayLevel>(c => 
        async (level, resultHandler) => {
            try {
                await Task.Run(() => {
                    var enumerator = level.RebuildProcessedData();
                    while (enumerator.MoveNext()) ;
                });
                var gameSetupScope = new Scope();
                //TODO: Add elements that should be resolved from core scope here
                GameRegistrators.RegisterSetupScope(gameSetupScope);
                gameSetupScope.Transfer<IsTilePassible>(c);
                var Game = new Game(level, gameSetupScope.Resolve<GameRulesFactory>());
                gameSetupScope.Dispose();
                var gameplayScope = new Scope();
                GameRegistrators.RegisterGameplayScope(gameplayScope);
                gameplayScope.RegisterExternal(Game.onChanged);
                var coreRoot = c.Resolve<IGameRoot>();
                coreRoot.SetGameContent(gameplayScope.Resolve<GameContentViewModelFactory>()());
                gameplayScope.Resolve<SetupUI>().Invoke(coreRoot.uiViewModel);
                await Task.Run(() => {
                    while (Game.state.results == null) {
                        System.Threading.Thread.Sleep(System.TimeSpan.FromSeconds(0.5f));
                    }
                });
                gameplayScope.Dispose();
                resultHandler(Game.state.results);
            }
            catch (Exception exc) {
                Debug.LogError(exc.Message + '\n' + exc.StackTrace);
            }
        });

        coreScope.RegisterScoped<HandleGameResults>(c =>
        (result)=> {
            // This should handle logic when a level is completed. Should it automatically start next level? Or return to menu.

            if (result.IsVictory) {
                c.Resolve<MarkLevelPassed>().Invoke(result.playedLevel.ID);
                c.Resolve<INextLevelLocator>().FindNextLevel(result.playedLevel.ID);
                //Level beaten. Write flag of progress to core state
            } else {
                // Player gave up. Return to menu.
                c.Resolve<StartMenu>().Invoke();
            }
        });

        coreScope.RegisterScoped<StartMenu>(c=>
        async () => {
            try {
                var MenuScope = new Scope();
                var root = c.Resolve<IGameRoot>();
                MenuScope.RegisterExternal(root);
                MenuScope.RegisterExternal(c.Resolve<ResetProgress>());
                MenuScope.RegisterExternal(c.Resolve<RunGameplayLevel>());
                MenuScope.Transfer<HandleGameResults>(c);
                MenuScope.Transfer<GetAllLevels>(c);
                MenuScope.Transfer<GetLevelRank>(c);
                MenuScope.Transfer < IsLevelPassed>(c);
                MenuScope.RegisterScoped<SetTopBarContent>(c => root.uiViewModel.SetUITopViewModel);
                MenuScope.RegisterScoped<SetBottomBarContent>(c => root.uiViewModel.SetUIBottomViewModel);
                MenuScope.RegisterScoped<SetMainUIContent>(c => root.uiViewModel.SetUICenterViewModel);

                MenuRegistrators.RegisterMenuScope(MenuScope);

                root.SetGameContent(MenuScope.Resolve<InitialContent>().Invoke());
                MenuScope.Resolve<SetupUI>().Invoke(root.uiViewModel);
            }
            catch (Exception exc) {
                Debug.LogError(exc.Message + '\n' + exc.StackTrace);
            }
        });
    }
}