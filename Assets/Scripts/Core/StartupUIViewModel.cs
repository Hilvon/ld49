using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public interface ILoadingScreen
{
    IObservable<float> progress { get; }
    IObservable<string> status { get; }
}

public interface ILoadingProgressTracker {
    void SetProgressAmount(float progress);
    void SetStatus(string status);
}


public class StartupUIViewModel : DisposableCollection, ILoadingScreen, ILoadingProgressTracker
{
    public IObservable<float> progress => _progress;
    public IObservable<string> status => _status;

    private Subject<float> _progress;
    private Subject<string> _status;

    public StartupUIViewModel() {
        _progress = new Subject<float>().AddTo(this);
        _status = new Subject<string>().AddTo(this);
    }

    public void SetProgressAmount(float progress) {
        _progress.OnNext(progress);
    }

    public void SetStatus(string status) {
        _status.OnNext(status);
    }
}
