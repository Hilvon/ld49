using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scope : DisposableCollection, IScopeResolver
{
    private Dictionary<Type, (object instance, bool isManaged)> createdInstances = new Dictionary<Type, (object instance, bool isManaged)>();
    private Dictionary<Type, Func<IScopeResolver, object>> constructors = new Dictionary<Type, Func<IScopeResolver, object>>();

    public T Resolve<T>() {
        if (createdInstances.TryGetValue(typeof(T), out var instanceData)) {
            return (T)instanceData.instance;
        }
        if (constructors.TryGetValue(typeof(T), out var constructor)) {
            var newInstance = constructor?.Invoke(this);
            if (newInstance != null && newInstance is T typed) {
                createdInstances.Add(typeof(T), (newInstance, true));
                return typed;
            }
        }
        Debug.LogError($"FailedToResolve {typeof(T)}");
        throw new Exception($"Unable to resolve type {typeof(T).Name}. Please check if it is registerred");
    }

    public void RegisterScoped<T>(Func<IScopeResolver, T> constructor) {
        if (constructors.ContainsKey(typeof(T)))
            throw new Exception($"Factory for type {typeof(T).Name} already registerred. Please check for double registation");
        constructors.Add(typeof(T),c => {
            if (constructor == null) return null;
            return constructor(c); 
        });
    }

    public void RegisterExternal(object instance) {
        if (createdInstances.ContainsKey(instance.GetType())) {
            throw new Exception($"Instance of {instance.GetType().Name} already scoped prior to external registering. Please check if you are double registering.");
        }
        createdInstances.Add(instance.GetType(), (instance, false));
    }
    public void RegisterExternal<T>(T instance) {
        if (createdInstances.ContainsKey(typeof(T))) {
            throw new Exception($"Instance of {typeof(T).Name} already scoped prior to external registering. Please check if you are double registering.");
        }
        createdInstances.Add(typeof(T), (instance, false));
    }
    public override void Dispose() {
        foreach (var instanceData in createdInstances.Values) {
            if (instanceData.isManaged && instanceData.instance is IDisposable disposable) {
                disposable.Dispose();
            }
        }
        base.Dispose();
    }
}

public interface IScopeResolver
{
    T Resolve<T>();
}

public static class ScopeHelper
{
    public static void Transfer<T>(this Scope to, IScopeResolver from) {
        to.RegisterExternal<T>(from.Resolve<T>());
    }
}