using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class View<T> : View
{
    protected sealed override void BindInternal(object viewModel, ISUbviewFactory subviewBinder, DisposableCollection disposableCollection) {
        if (viewModel is T typed) {
            BindInternal(typed, subviewBinder, disposableCollection);
        }
    }
    public sealed override bool CanBind(object viewModel) {
        return viewModel is T;
    }
    protected abstract void BindInternal(T viewModel, ISUbviewFactory subviewBinder, DisposableCollection disposableCollection);
}

public abstract class View: MonoBehaviour
{
    private DisposableCollection _internalDisposable;
    public void Bind(object viewModel, ISUbviewFactory subviewBinder, DisposableCollection disposableCollection) {
        _internalDisposable = new DisposableCollection();
        disposableCollection.Add(_internalDisposable);
        BindInternal(viewModel, subviewBinder, _internalDisposable);
    }

    public abstract bool CanBind(object viewModel);

    protected abstract void BindInternal(object viewModel, ISUbviewFactory subviewBinder, DisposableCollection disposableCollection);
    protected virtual void OnDestroy() {
        _internalDisposable.Dispose();
    }
}

public static class ViewExtensions
{
    public static void BindViews(this GameObject instance, object viewModel, ISUbviewFactory subviewBinder, DisposableCollection disposableCollection) {
        foreach (var view in instance.GetComponents<View>()) {
            if (view.CanBind(viewModel)) {
                view.Bind(viewModel, subviewBinder, disposableCollection);
            }
        }
    }
}
