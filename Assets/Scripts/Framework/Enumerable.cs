using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EnumerableExtensions
{
    public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action) {
        if (action == null)
            return;
        foreach (var item in enumerable) {
            action(item);
        }
    }
}
