using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubviewFactory : ISUbviewFactory
{
    private Dictionary<Type, GameObject> prefabTemplates = new Dictionary<Type, GameObject>();
    public void BindSubview(IObservable<object> viewModel, GameObject template, Transform host, DisposableCollection disposableCollection) {
        var serialDisposable = new SerialDisposable().AddTo(disposableCollection);
        GameObject currentView = null;
        viewModel.Subscribe(obj => {
            if (currentView!= null) {
                Cleanup(currentView);
                serialDisposable.current = null;
            }
            if (obj == null)
                return;
            var newCollection = new DisposableCollection().SetTo(serialDisposable);
            currentView = FetchInstance(obj, template, host, newCollection);
        }).AddTo(disposableCollection);
    }

    private GameObject FetchInstance(object viewModel, GameObject template, Transform host, DisposableCollection disposableCollection) {
        if (template == null) {
            if (!prefabTemplates.TryGetValue(viewModel.GetType(), out template)) {
                template = Resources.Load<GameObject>(viewModel.GetType().Name.Replace("ViewModel", ""));
                prefabTemplates.Add(viewModel.GetType(), template);
            }
        }
        if (template == null) {
            throw new Exception($"Unable to locate prefab template to bind subview {viewModel.GetType().Name}");
        }
        var instance = GameObject.Instantiate(template, host, false);
        instance.BindViews(viewModel, this, disposableCollection);
        return instance;
    }

    private void Cleanup(GameObject instance) {
        GameObject.Destroy(instance); //TODO: This might be pullins solution if neccesary.
    }
}

public interface ISUbviewFactory
{
    void BindSubview(IObservable<object> viewModel, GameObject template, Transform host, DisposableCollection disposableCollection);
}

public static class ISUbviewFactoryExtensions
{
    public static void BindSubview(this ISUbviewFactory factory, object viewModel, GameObject template, Transform host, DisposableCollection disposableCollection) {
        factory.BindSubview(Observable.Return(viewModel), template, host, disposableCollection);
    }
}
