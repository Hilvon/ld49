using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class WorldModel : IReadOnlyWorldState
{
    public readonly CharacterModel characterModel;
    public readonly IGameLevelData gameLevelData;
    public IReadonlyPlayerState playerState => characterModel;
    public int timeStep => MovementHistory.Count;
    public List<int> MovementHistory = new List<int>();
    public readonly GameUIState GameUI;
    public IUIState uiState => GameUI;

    public GameResults results;

    public int fieldWidth => gameLevelData.width;

    public int fieldHeight => gameLevelData.height;

    public WorldModel(IGameLevelData levelData) {
        GameUI = new GameUIState();
        this.characterModel = new CharacterModel();
        gameLevelData = levelData;
        this.characterModel.CharacterPosition = levelData.initialPosition;
    }

    public int GetTileType(int x, int y, int deltaT) {
        return gameLevelData.GetTileType(x, y, timeStep + deltaT);
    }
}

public interface IReadOnlyWorldState
{
    IReadonlyPlayerState playerState { get; }
    IUIState uiState { get; }
    int timeStep { get; }
    int fieldWidth { get; }
    int fieldHeight { get; }
    int GetTileType(int x, int y, int deltaT);
}