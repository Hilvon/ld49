using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate IRules GameRulesFactory(IModifiableWorld world);
public class GameRules: IRules
{     
    public IPlayerMovementRules movementRules { get; }

    public GameRules(
        IModifiableWorld world,
        PlayerMovementRulesFactory movementRulesFactory)
    {
        movementRules = movementRulesFactory(world);
    }
}
public interface IRules
{
    IPlayerMovementRules movementRules { get; }
}

