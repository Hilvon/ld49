using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public delegate Task RunGameplayLevel(LevelDataHolder levelData, HandleGameResults gameResultHandler);

public class Game : IGame, IModifiableWorld
{
    public IRules rules { get; }
    public IReadOnlyWorldState worldState => state;
    public IReadOnlyReactiveProperty<IGame> onChanged => _onChanged;
    public WorldModel state { get; }

    private readonly Subject<IGame> _onChanged;

    public Game(IGameLevelData levelData, GameRulesFactory gameRulesFactory) {
        var _world = new WorldModel(levelData);
        state = _world;
        rules = gameRulesFactory(this);
        _onChanged = new Subject<IGame>();
        _onChanged.OnNext(this);
    }
    public void AnnounceChanged() {
        _onChanged.OnNext(this);
    }
}

public interface IGame
{
    IRules rules { get; }
    IReadOnlyWorldState worldState { get; }
}

public interface IModifiableWorld
{
    WorldModel state { get; }
    IRules rules { get; }
    void AnnounceChanged();
}

public interface IUIState
{
    bool ShowForecast { get; }
}