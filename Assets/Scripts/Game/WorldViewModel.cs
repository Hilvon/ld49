using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public delegate IWorldViewModel WorldViewModelFactory();
public interface IWorldViewModel : IDisposable {
    List<IWorldTileViewModel> tiles { get; }
}
public class WorldViewModel: DisposableCollection, IWorldViewModel
{
    public List<IWorldTileViewModel> tiles { get; }
    public WorldViewModel(IReadOnlyReactiveProperty<IGame> game, WorldTileViewModelFactory tileViewModelFactory) {
        tiles = Enumerable.Range(0, game.value.worldState.fieldWidth)
            .SelectMany(x => Enumerable.Range(0, game.value.worldState.fieldHeight).Select(y => new Vector2Int(x, y)))
            .Select(pos => tileViewModelFactory(pos.x, pos.y)).ToList();
            
    }
}
