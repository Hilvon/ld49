using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameRegistrators
{
    public static void RegisterSetupScope(Scope GameStartupScope) {

        GameStartupScope.RegisterScoped<PlayerMovementRulesFactory>(c =>
            (world) => new PlayerMovementRules(
                world,
                c.Resolve<IsTilePassible>())
        );

        GameStartupScope.RegisterScoped<GameRulesFactory>(c =>
            (world) => new GameRules(
                world,
                c.Resolve<PlayerMovementRulesFactory>())
        );
    }

    public static void RegisterGameplayScope(Scope GameplayScope) {
        GameplayScope.RegisterScoped<GameContentViewModelFactory>(c =>
            () => new GameContentViewModel(
                c.Resolve<WorldViewModelFactory>(),
                c.Resolve<PlayerViewModelFactory>())
        );

        GameplayScope.RegisterScoped<SetupUI>(c =>
            uiControl => {
                uiControl.SetUICenterViewModel(null);
                uiControl.SetUITopViewModel(null);
                uiControl.SetUIBottomViewModel(null);
        });

        GameplayScope.RegisterScoped<WorldViewModelFactory>(c =>
            () => new WorldViewModel(c.Resolve<IReadOnlyReactiveProperty<IGame>>(),
                c.Resolve<WorldTileViewModelFactory>())
        );

        GameplayScope.RegisterScoped<PlayerViewModelFactory>(c =>
            ()=> new PlayerViewModel(c.Resolve<IReadOnlyReactiveProperty<IGame>>())
        );

        GameplayScope.RegisterScoped<WorldTileViewModelFactory>(c=>
            (x,y)=> new WorldTileViewModel(
                c.Resolve<IReadOnlyReactiveProperty<IGame>>(),
                x,
                y)
        );
    }
}
