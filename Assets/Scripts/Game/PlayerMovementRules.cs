using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate IPlayerMovementRules PlayerMovementRulesFactory(IModifiableWorld world);
public class PlayerMovementRules : IPlayerMovementRules
{
    private readonly WorldModel _world;
    private readonly IsTilePassible _PassibilityQuery;
    private Action onModified;
    public PlayerMovementRules(IModifiableWorld world, IsTilePassible passibilityQuery) {
        _world = world.state;
        onModified = world.AnnounceChanged;
        this._PassibilityQuery = passibilityQuery;
    }
    public void MovePlayerDown() {
        HandleMovement(4);
    }

    public void MovePlayerLeft() {
        HandleMovement(1);
    }

    public void MovePlayerRight() {
        HandleMovement(3);
    }

    public void MovePlayerUp() {
        HandleMovement(2);
    }

    public void Rewind() {
        if (_world.MovementHistory.Count == 0)
            return;
        var lastMoveDirection = _world.MovementHistory[_world.MovementHistory.Count - 1];
        var offset = directionToOffset(lastMoveDirection) * -1;
        _world.characterModel.CharacterPosition -= offset;
        _world.MovementHistory.RemoveAt(_world.MovementHistory.Count - 1);
        onModified?.Invoke();
    }

    private void HandleMovement(int direction) {
        var destinationPosition = _world.characterModel.CharacterPosition + directionToOffset(direction);

        if (_PassibilityQuery(_world.GetTileType(destinationPosition.x, destinationPosition.y, 0))) {
            _world.characterModel.CharacterPosition = destinationPosition;
            _world.MovementHistory.Add(direction);
        } else {
            _world.MovementHistory.Add(0);
        }
        onModified?.Invoke();
    }

    private static Vector2Int directionToOffset(int direction) {
        switch (direction) {
            case 1: return Vector2Int.left;
            case 2: return Vector2Int.up;
            case 3: return Vector2Int.right;
            case 4: return Vector2Int.down;
        }
        return Vector2Int.zero;
    }
}

public interface IPlayerMovementRules
{
    void MovePlayerUp();
    void MovePlayerDown();
    void MovePlayerLeft();
    void MovePlayerRight();
    void Rewind();
}