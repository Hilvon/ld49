using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUIState : IUIState
{
    public bool ShowForecast;
    bool IUIState.ShowForecast => ShowForecast;
}
