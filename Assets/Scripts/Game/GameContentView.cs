using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameContentView : View<GameContentViewModel>
{
    
    protected override void BindInternal(GameContentViewModel viewModel, ISUbviewFactory subviewBinder, DisposableCollection disposableCollection) {
        subviewBinder.BindSubview(viewModel.world, null, transform, disposableCollection);
    }
}
