using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game/LevelDataHolder")]
public class LevelDataHolder : ScriptableObject, IGameLevelData
{
    [SerializeField] private GameLevelData _data;

    [SerializeField] private TileTypeDecoder _decoder;

    [SerializeField] private LevelDataHolder _RequiredLevel;

    public int requiredLevelId=> _RequiredLevel?.ID??-1;
    public int ID => name.GetHashCode();

    private int[] processedArray;

    Vector2Int IGameLevelData.initialPosition => _data.InitialPosition;

    int IGameLevelData.width => _data.FieldSize.x;

    int IGameLevelData.height => _data.FieldSize.y;

    public IEnumerator RebuildProcessedData() {
        processedArray = new int[_data.FieldSize.x * _data.FieldSize.y * _data.Depth];
        for (int t = 0; t < _data.Depth; t++) {
            for (int y = 0; y < _data.FieldSize.y; y++) {
                for (int x = 0; x < _data.FieldSize.x; x++) {
                    processedArray[CombineIndex(x, y, t, _data)] = _decoder.DecodeTileType(_data.GetTileType(x, y, t));
                    yield return null;
                }
            }
        }
    }

    private static int CombineIndex(int x, int y, int t, GameLevelData data) {
        return x + data.FieldSize.x * (y + data.FieldSize.y * t);
    }

    public int GetTileType(int x, int y, int t) {
        if (x < 0 || x >= _data.FieldSize.x || y < 0 || y <= _data.FieldSize.y)
            return _decoder.DecodeTileType(Color.clear);
        if (processedArray == null) {
            var fillDataEnumerator = RebuildProcessedData();
            while (fillDataEnumerator.MoveNext()) { }
        }
        return processedArray[CombineIndex(x, y, t, _data)];
    }

    int IGameLevelData.GetTileType(int x, int y, int t) {
        throw new System.NotImplementedException();
    }
}
