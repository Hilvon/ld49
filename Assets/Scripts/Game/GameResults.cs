using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameResults 
{
    public bool IsVictory;
    public LevelDataHolder playedLevel;
}
