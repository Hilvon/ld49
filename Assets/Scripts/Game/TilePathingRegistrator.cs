using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilePathingRegistrator : ConfigRegistrationBase
{
    [SerializeField] private TilePathingHandler _pathingManager;
    public override void RegisterInScope(Scope scope) {
        scope.RegisterScoped<IsTilePassible>(c => _pathingManager.IsTilePassable);
    }
}
