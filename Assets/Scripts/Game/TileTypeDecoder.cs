using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(menuName = "Game/TileTypeDecoder")]
public class TileTypeDecoder : ScriptableObject
{
    [SerializeField] private ColorMarkerDescription[] descriptors;
    [SerializeField] private int NullTileType = -1;
    public int DecodeTileType(Color pixelColor) {
        if (pixelColor.a <= 0.01f || descriptors.Length == 0)
            return NullTileType;
        
        return descriptors.OrderBy(x => Distance(pixelColor, x.color)).FirstOrDefault().type;
    }

    private static float Distance(Color a, Color b) {
        return Vector3.SqrMagnitude(new Vector3(a.r, a.g, a.b) - new Vector3(b.r, b.g, b.b));
    }

    [System.Serializable]
    private struct ColorMarkerDescription
    {
        public int type;
        public Color color;
    }
}
