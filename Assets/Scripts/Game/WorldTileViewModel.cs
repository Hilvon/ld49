using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public delegate IWorldTileViewModel WorldTileViewModelFactory(int x, int y);

public interface IWorldTileViewModel: IDisposable
{
    IObservable<int> TileTypeIndex { get; }
    IObservable<int> TileTypeIn1Index { get; }
    IObservable<int> TileTypeIn2Index { get; }
    IObservable<int> TileTypeIn3Index { get; }
}

public class WorldTileViewModel: DisposableCollection, IWorldTileViewModel
{
    public IObservable<int> TileTypeIndex { get; }
    public IObservable<int> TileTypeIn1Index { get; }
    public IObservable<int> TileTypeIn2Index { get; }
    public IObservable<int> TileTypeIn3Index { get; }

    public IObservable<bool> TileForecase { get; }

    public WorldTileViewModel(IReadOnlyReactiveProperty<IGame> world, int x, int y) {
        this.TileTypeIndex = world.Select(w => w.worldState.GetTileType(x, y, 0)).DistinctUntilChanged();

        this.TileTypeIn1Index = world.Select(w => w.worldState.GetTileType(x, y, 1)).DistinctUntilChanged();

        this.TileTypeIn2Index = world.Select(w => w.worldState.GetTileType(x, y, 2)).DistinctUntilChanged();

        this.TileTypeIn3Index = world.Select(w => w.worldState.GetTileType(x, y, 3)).DistinctUntilChanged();

        this.TileForecase = world.Select(w => w.worldState.uiState.ShowForecast);
    }
}
