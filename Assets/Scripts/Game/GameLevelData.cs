using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public interface IGameLevelData
{
    int GetTileType(int x, int y, int t);
    int width { get; }
    int height { get; }

    Vector2Int initialPosition { get; }
}

[System.Serializable]
public class GameLevelData 
{
    public Vector2Int FieldSize;
    public Vector2Int InitialPosition;
    public int Depth => _fieldImages.Length;
    [SerializeField] private Sprite[] _fieldImages;

    
    public Color GetTileType(int x, int y, int t) {
        while (t >= _fieldImages.Length)
            t--;
        var curColor = GetSpriteColor(_fieldImages[t], x, y);
        while (t>0 && curColor.a <=0.01f) {
            t--;
            curColor = GetSpriteColor(_fieldImages[t], x, y);
        }
        return curColor;
        //return _decoder.DecodeTileType(curColor);
    }

    private Color GetSpriteColor(Sprite sprite, int x, int y) {
        var textureRect = sprite.textureRect;
        var pos = textureRect.center - textureRect.size / 2;
        return sprite.texture.GetPixel((int)pos.x + x, (int)pos.y + y);
    }
}
