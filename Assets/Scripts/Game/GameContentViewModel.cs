using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate GameContentViewModel GameContentViewModelFactory();

public class GameContentViewModel : DisposableCollection
{
    public IWorldViewModel world;
    public IPlayerViewModel player;
    public GameContentViewModel(
        WorldViewModelFactory worldViewModelFactory,
        PlayerViewModelFactory playerViewModelFactory) {
        this.world = worldViewModelFactory();
        this.player = playerViewModelFactory();
    }
}
