using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(menuName ="Game/PathingHandler")]
public class TilePathingHandler : ScriptableObject
{
    [SerializeField] private int[] impassibleTypes;

    public bool IsTilePassable(int tileType) {
        return impassibleTypes.Any(x => x == tileType);
    }

   
}

public delegate bool IsTilePassible(int tyleType);
