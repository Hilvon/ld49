using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterModel: IReadonlyPlayerState
{
    public Vector2Int CharacterPosition;
    public bool CharacterAlive;

    public Vector2Int PlayerPosition => CharacterPosition;

    public bool PlayerAlive => CharacterAlive;
}

public interface IReadonlyPlayerState
{
    Vector2Int PlayerPosition { get; }
    bool PlayerAlive { get; }
}