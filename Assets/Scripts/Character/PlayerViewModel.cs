using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate IPlayerViewModel PlayerViewModelFactory();
public interface IPlayerViewModel: IDisposable
{
    IObservable<Vector2Int> position { get; }
    IObservable<bool> isAlive { get; }
    IPlayerMovementRules moveRules { get; }
}
public class PlayerViewModel : DisposableCollection, IPlayerViewModel
{
    public IObservable<Vector2Int> position { get; }
    public IObservable<bool> isAlive { get; }

    public IPlayerMovementRules moveRules { get; }

    public PlayerViewModel(IReadOnlyReactiveProperty<IGame> game) {
        this.position = game.Select(g => g.worldState.playerState.PlayerPosition);
        this.isAlive = game.Select(g => g.worldState.playerState.PlayerAlive);
        moveRules = game.value.rules.movementRules;
    }
}
