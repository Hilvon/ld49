using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MenuRegistrators
{
    public static void RegisterMenuScope(Scope menuScope) {
        menuScope.RegisterScoped<InitialContent>(c =>
            () => null
        );

        menuScope.RegisterScoped<SetupUI>(c =>
            (uiRoot) => {
                uiRoot.SetUICenterViewModel(c.Resolve<MainMenuViewModelFactory>().Invoke());
        });

        menuScope.RegisterScoped<LevelSelectViewModelFactory>(c =>
            (onClose) => new LevelSelectViewModel(
                c.Resolve<GetAllLevels>(),
                c.Resolve<SelectableLevelViewModelFactory>(),
                onClose)
        );

        menuScope.RegisterScoped<MainMenuViewModelFactory>(c =>
            () => new MainMenuViewModel(
                c.Resolve<SetMainUIContent>(),
                c.Resolve<LevelSelectViewModelFactory>(),
                c.Resolve<ResetProgress>())
        );

        menuScope.RegisterScoped<SelectableLevelViewModelFactory>(c =>
            level => new SelectableLevelViewModel(
                c.Resolve<RunGameplayLevel>(),
                c.Resolve<HandleGameResults>(),
                level,
                c.Resolve<GetLevelRank>(),
                c.Resolve<IsLevelPassed>())
        );
    }
}