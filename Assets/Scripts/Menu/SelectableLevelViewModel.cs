using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate SelectableLevelViewModel SelectableLevelViewModelFactory(LevelDataHolder level);
public class SelectableLevelViewModel : DisposableCollection
{
    public string Label { get; }
    public ReactiveCommand<int> OnSelected { get; }

    public SelectableLevelViewModel(RunGameplayLevel levelStarter, HandleGameResults resultsHandler, LevelDataHolder level, GetLevelRank rankEvaluator, IsLevelPassed passChecker) {
        Label = rankEvaluator(level).ToString();
        OnSelected = new ReactiveCommand<int>(passChecker(level.requiredLevelId)).AddTo(this);
        OnSelected.Subscribe(_ => levelStarter(level, resultsHandler)).AddTo(this);
    }
}
