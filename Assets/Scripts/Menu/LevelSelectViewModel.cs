using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public delegate LevelSelectViewModel LevelSelectViewModelFactory(Action onClose);

public class LevelSelectViewModel : DisposableCollection 
{
    public List<SelectableLevelViewModel> levels { get; }
    public ReactiveCommand<int> close { get; }

    public LevelSelectViewModel(GetAllLevels levelsFetcher, SelectableLevelViewModelFactory selectableFactory, Action onClose) {
        close = new ReactiveCommand<int>().AddTo(this);
        close.Subscribe(_ => onClose()).AddTo(this);

        levels = levelsFetcher().Select(x => selectableFactory(x).AddTo(this)).ToList();
    }
}
