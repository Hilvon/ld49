using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuView : View<IMainMenuViewModel>
{
    [SerializeField] private Button startGame;
    [SerializeField] private Button resetGame;
    [SerializeField] private Button exitGame;
    protected override void BindInternal(IMainMenuViewModel viewModel, ISUbviewFactory subviewBinder, DisposableCollection disposableCollection) {
        startGame.Bind<int>(viewModel.OnStartPlay, disposableCollection);
        resetGame.Bind<int>(viewModel.OnResetProgress, disposableCollection);
        exitGame.Bind<int>(viewModel.OnExit, disposableCollection);
    }
}
