using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SelectableLevelView : View<SelectableLevelViewModel>
{
    [SerializeField] TMP_Text label;
    [SerializeField] Button playLevel;
    protected override void BindInternal(SelectableLevelViewModel viewModel, ISUbviewFactory subviewBinder, DisposableCollection disposableCollection) {
        playLevel.Bind(viewModel.OnSelected, disposableCollection);
        label.text = viewModel.Label;
    }
}
