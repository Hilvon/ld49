using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public delegate IMainMenuViewModel MainMenuViewModelFactory();

public interface IMainMenuViewModel
{
    ReactiveCommand<int> OnStartPlay { get; }
    ReactiveCommand<int> OnExit { get; }
    ReactiveCommand<int> OnResetProgress { get; }
}

public class MainMenuViewModel : DisposableCollection, IMainMenuViewModel
{
    public ReactiveCommand<int> OnStartPlay { get; }
    public ReactiveCommand<int> OnExit { get; }
    public ReactiveCommand<int> OnResetProgress { get; }

    public MainMenuViewModel(SetMainUIContent mainUIControl, LevelSelectViewModelFactory levelSelectFactory, ResetProgress resetProgress) {
        this.OnStartPlay = new ReactiveCommand<int>().AddTo(this);
        this.OnStartPlay.Subscribe(_ => {
            mainUIControl(levelSelectFactory(()=> mainUIControl(this)));
        }).AddTo(this);

        this.OnExit = new ReactiveCommand<int>().AddTo(this);
        this.OnExit.Subscribe(_ => {
            Application.Quit();
        }).AddTo(this);

        this.OnResetProgress = new ReactiveCommand<int>().AddTo(this);
        this.OnResetProgress.Subscribe(_ => {
            resetProgress();
        }).AddTo(this);

    }

    public override void Dispose() {
        base.Dispose();
        Debug.Log("Main Menu View Model disposing");
    }
}
