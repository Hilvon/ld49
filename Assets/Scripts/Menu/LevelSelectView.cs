using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectView : View<LevelSelectViewModel>
{
    [SerializeField] Button close;
    [SerializeField] RectTransform LevelsList;

    protected override void BindInternal(LevelSelectViewModel viewModel, ISUbviewFactory subviewBinder, DisposableCollection disposableCollection) {
        close.Bind(viewModel.close, disposableCollection);

        viewModel.levels.ForEach(lvl => subviewBinder.BindSubview(lvl, null, LevelsList, disposableCollection));
    }
}
