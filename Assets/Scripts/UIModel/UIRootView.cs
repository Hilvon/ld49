using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIRootView : View<IUIRootViewModel>
{
    [SerializeField] private Transform contentHost;
    [SerializeField] private Transform topBarHost;
    [SerializeField] private Transform bottomBarHost;
    protected override void BindInternal(IUIRootViewModel viewModel, ISUbviewFactory subviewBinder, DisposableCollection disposableCollection) {
        subviewBinder.BindSubview(viewModel.contentViewModel, null, contentHost, disposableCollection);
        subviewBinder.BindSubview(viewModel.topBarViewModel, null, topBarHost, disposableCollection);
        subviewBinder.BindSubview(viewModel.bottomBarViewModel, null, bottomBarHost, disposableCollection);
    }
}
