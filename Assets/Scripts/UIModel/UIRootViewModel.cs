using System;

public interface IUIRootViewModel
{
    IObservable<object> contentViewModel { get; }
    IObservable<object> topBarViewModel { get; }
    IObservable<object> bottomBarViewModel { get; }
}

public interface IUIControl
{
    void SetUITopViewModel(object viewModel);
    void SetUIBottomViewModel(object viewModel);
    void SetUICenterViewModel(object viewModel);
}

public class UIRootViewModel: DisposableCollection, IUIRootViewModel, IUIControl
{
    public IObservable<object> contentViewModel => _contentViewModel;
    public IObservable<object> topBarViewModel => _topBarViewModel;
    public IObservable<object> bottomBarViewModel => _bottomBarViewModel;

    private Subject<object> _contentViewModel;
    private Subject<object> _topBarViewModel;
    private Subject<object> _bottomBarViewModel;

    public UIRootViewModel(object initialContent, object initialTopBar = null, object initialBottomBar = null) {
        this._contentViewModel = new Subject<object>(initialContent)            
            .AddTo(this);

        this._topBarViewModel = new Subject<object>(initialTopBar)
            .AddTo(this);

        this._bottomBarViewModel = new Subject<object>(initialBottomBar)
            .AddTo(this);
    }

    public void SetUITopViewModel(object viewModel) {
        _topBarViewModel.OnNext(viewModel);
    }

    public void SetUIBottomViewModel(object viewModel) {
        _bottomBarViewModel.OnNext(viewModel);
    }

    public void SetUICenterViewModel(object viewModel) {
        _contentViewModel.OnNext(viewModel);
    }
}

public delegate void SetTopBarContent(object viewModel);
public delegate void SetBottomBarContent(object viewModel);
public delegate void SetMainUIContent(object viewModel);
